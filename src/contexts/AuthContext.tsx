import React, { createContext, useContext, useState } from 'react';

interface AuthContextTypes {
  isLoggedIn: boolean;
  loginUser: () => void;
  logoutUser: () => void;
}

interface AuthContextProviderTypes {
  children: React.ReactNode;
}

const AuthContext = createContext<AuthContextTypes>({} as AuthContextTypes);

const AuthProvider: React.FC<AuthContextProviderTypes> = ({ children }) => {
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  const loginUser = () => {
    setIsLoggedIn(true);
  };

  const logoutUser = () => {
    setIsLoggedIn(false);
  };

  return (
    <AuthContext.Provider
      value={{
        isLoggedIn,
        loginUser,
        logoutUser,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};

export default AuthProvider;

export const useAuthContext = () => {
  const context = useContext(AuthContext);

  if (!context) {
    throw new Error('Auth context must be used inside Auth Context Provider');
  }

  return context;
};
