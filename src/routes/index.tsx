import React from 'react';
import { useAuthContext } from '../contexts/AuthContext';
import AppRoutes from './AppRoutes';
import AuthRoutes from './AuthRoutes';

const Routes = () => {
  const { isLoggedIn } = useAuthContext();

  return isLoggedIn ? <AppRoutes /> : <AuthRoutes />;
};

export default Routes;
