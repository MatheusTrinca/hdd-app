import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Home from '../screens/Home';

type AppRouteParams = {
  Home: undefined;
};

const AppRoutes = () => {
  const App = createNativeStackNavigator<AppRouteParams>();

  return (
    <App.Navigator>
      <App.Screen name="Home" component={Home} />
    </App.Navigator>
  );
};

export default AppRoutes;
