import React from 'react';
import Signin from '../screens/Signin';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Signup from '../screens/Signup';

export type AuthStackParams = {
  Signin: undefined;
  Signup: undefined;
};

const AuthRoutes = () => {
  const Auth = createNativeStackNavigator<AuthStackParams>();

  return (
    <Auth.Navigator screenOptions={{ headerShown: false }}>
      <Auth.Screen name="Signin" component={Signin} />
      <Auth.Screen name="Signup" component={Signup} />
    </Auth.Navigator>
  );
};

export default AuthRoutes;
