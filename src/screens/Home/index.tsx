import { View, Text, Button } from 'react-native';
import React from 'react';
import { useAuthContext } from '../../contexts/AuthContext';

const Home = () => {
  const { logoutUser } = useAuthContext();

  return (
    <View>
      <Text>Home</Text>
      <Button title="Log out" onPress={logoutUser} />
    </View>
  );
};

export default Home;
