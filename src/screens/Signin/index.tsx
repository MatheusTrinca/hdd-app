import React, { useState } from 'react';
import {
  AccountContainer,
  Container,
  AccountButton,
  AccountIcon,
  AccountText,
  ForgotPasswordButton,
  ForgotPasswordText,
  LogoImage,
  Title,
} from './styles';
import logo from '../../../assets/images/logo.png';
import AuthInput from '../../components/inputs/AuthInput';
import MainButton from '../../components/buttons/MainButton';
import { validateEamail } from '../../utils/validateEmail';
import { validatePassword } from '../../utils/validatePassword';
import { useNavigation } from '@react-navigation/native';
import { AuthStackParams } from '../../routes/AuthRoutes';
import { NativeStackNavigationProp } from '@react-navigation/native-stack';
import { useAuthContext } from '../../contexts/AuthContext';

const Signin = () => {
  const { loginUser } = useAuthContext();

  const navigation =
    useNavigation<NativeStackNavigationProp<AuthStackParams>>();

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState({
    type: 'mail' || 'password',
    message: '',
  });

  const submitHandler = () => {
    if (!validateEamail(email)) {
      setError({ type: 'mail', message: 'Email inválido' });
      return;
    }

    if (!validatePassword(password)) {
      setError({
        type: 'password',
        message: 'Senha deve conter no mínimo 6 caracteres',
      });
      return;
    }
    console.log(email, password);
    loginUser();
  };

  const handleEmailChange = (text: string) => {
    if (error.type === 'mail' && validateEamail(email)) {
      setError({ type: '', message: '' });
    }
    setEmail(text);
  };

  const handlePasswordChange = (text: string) => {
    if (error.type === 'password' && validatePassword(password)) {
      setError({ type: '', message: '' });
    }
    setPassword(text);
  };

  return (
    <>
      <Container>
        <LogoImage source={logo} />
        <Title>Faça seu login</Title>
        <AuthInput
          placeholder="E-Mail"
          icon="mail"
          value={email}
          onChangeText={handleEmailChange}
          errorMessage={error?.type === 'mail' ? error.message : undefined}
        />
        <AuthInput
          placeholder="Senha"
          icon="lock"
          value={password}
          onChangeText={handlePasswordChange}
          errorMessage={error?.type === 'password' ? error.message : undefined}
        />
        <MainButton title="Entrar" onPress={submitHandler} />
        <ForgotPasswordButton>
          <ForgotPasswordText>Esqueci minha senha</ForgotPasswordText>
        </ForgotPasswordButton>
      </Container>
      <AccountContainer>
        <AccountButton onPress={() => navigation.navigate('Signup')}>
          <AccountIcon name="log-in"></AccountIcon>
          <AccountText>Criar uma conta</AccountText>
        </AccountButton>
      </AccountContainer>
    </>
  );
};

export default Signin;
