import { Image } from 'react-native';
import styled from 'styled-components/native';
import { Feather } from '@expo/vector-icons';

export const Container = styled.View`
  flex: 1;
  padding: 0 40px;
  background-color: ${({ theme }) => theme.colors.background};
  align-items: center;
  justify-content: center;
`;

export const LogoImage = styled(Image)`
  width: 206px;
  height: 120px;
  margin-bottom: 60px;
`;

export const Title = styled.Text`
  color: ${({ theme }) => theme.colors.black};
  font-family: ${({ theme }) => theme.fonts.bold};
  font-size: 20px;
  color: ${({ theme }) => theme.colors.white};
  letter-spacing: 0.8px;
  margin-bottom: 24px;
`;

export const ForgotPasswordButton = styled.TouchableOpacity`
  margin-top: 12px;
`;

export const ForgotPasswordText = styled.Text`
  font-family: ${({ theme }) => theme.fonts.regular};
  font-size: 14px;
  color: ${({ theme }) => theme.colors.white};
`;

export const AccountContainer = styled.View`
  border: 1px solid ${({ theme }) => theme.colors.black900};
  padding: 20px 0;
  width: 100%;
  background-color: ${({ theme }) => theme.colors.background};
`;

export const AccountButton = styled.TouchableOpacity`
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;

export const AccountIcon = styled(Feather).attrs({
  size: 20,
})`
  color: ${({ theme }) => theme.colors.primary};
  margin-right: 14px;
`;

export const AccountText = styled.Text`
  font-family: ${({ theme }) => theme.fonts.regular};
  font-size: 14px;
  color: ${({ theme }) => theme.colors.primary};
`;
