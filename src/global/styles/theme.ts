export default {
  colors: {
    black: '#000',
    black900: '#232129',
    black800: '#3E3B47',
    background: '#312e38',
    primary: '#FF9000',
    white: '#F4EDE8',
    danger: 'red',
  },

  fonts: {
    regular: 'RobotoSlab_400Regular',
    bold: 'RobotoSlab_700Bold',
  },
};
