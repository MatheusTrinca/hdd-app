import styled from 'styled-components/native';
import { Feather } from '@expo/vector-icons';
import { TextInput } from 'react-native';

export const Container = styled.View`
  background-color: ${({ theme }) => theme.colors.black900};
  border-radius: 10px;
  width: 100%;
  max-width: 400px;
  height: 54px;
  flex-direction: row;
  align-items: center;
  padding: 0 16px;
  margin-bottom: 8px;
`;

export const Icon = styled(Feather).attrs({
  size: 24,
})`
  color: ${({ theme }) => theme.colors.black800};
  margin-right: 16px;
`;

export const Input = styled(TextInput)`
  font-family: ${({ theme }) => theme.fonts.bold};
  width: 80%;
  font-size: 16px;
  color: ${({ theme }) => theme.colors.black800};
`;

export const ErrorText = styled.Text`
  color: ${({ theme }) => theme.colors.danger};
  font-family: ${({ theme }) => theme.fonts.regular};
  font-size: 12px;
`;
