import React from 'react';
import { Text, TextInputProps } from 'react-native';
import theme from '../../../global/styles/theme';
import { Container, ErrorText, Icon, Input } from './styles';

interface AuthInputProps extends TextInputProps {
  icon: string;
  errorMessage: string | undefined;
}

const AuthInput: React.FC<AuthInputProps> = ({
  icon,
  errorMessage,
  ...props
}) => {
  return (
    <>
      <Container>
        <Icon name={icon} />
        <Input placeholderTextColor={theme.colors.black800} {...props} />
      </Container>
      {errorMessage && <ErrorText>{errorMessage}</ErrorText>}
    </>
  );
};

export default AuthInput;
