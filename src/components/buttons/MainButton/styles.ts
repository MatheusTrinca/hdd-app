import { TouchableOpacity } from 'react-native';
import styled from 'styled-components/native';

export const Container = styled(TouchableOpacity)`
  width: 100%;
  height: 54px;
  background-color: ${({ theme }) => theme.colors.primary};
  margin: 8px 0;
  border-radius: 8px;
  justify-content: center;
  align-items: center;
`;

export const ButtonTitle = styled.Text`
  color: ${({ theme }) => theme.colors.black};
  font-family: ${({ theme }) => theme.fonts.bold};
  font-size: 16px;
`;
