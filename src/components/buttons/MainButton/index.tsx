import React from 'react';
import { ButtonProps } from 'react-native';
import { ButtonTitle, Container } from './styles';

interface MainButtonProps extends ButtonProps {
  title: string;
}

const MainButton: React.FC<MainButtonProps> = ({ title, ...props }) => {
  return (
    <Container {...props}>
      <ButtonTitle>{title}</ButtonTitle>
    </Container>
  );
};

export default MainButton;
